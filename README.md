# VirtualStudio #

This is the source for a simple tool to use Visual Studio within a VM on Mac and retain Unity integration. It intercepts the open files / goto line numbers  from within Unity and passes them to Visual Studio inside the VM. It consists of 2 applications. This is the xCode project for the application on the Mac side. 

* To contact, visit www.quickfingers.net or @quickfingerz on Twitter.