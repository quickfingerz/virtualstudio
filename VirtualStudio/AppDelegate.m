//
//  AppDelegate.m
//  VirtualStudio
//
//  Created by Tom Jackson on 19/03/2012.
//  Copyright (c) 2012 Quick Fingers. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreServices/CoreServices.h>
@implementation AppDelegate

@synthesize window = _window;
@synthesize filesToOpenArray;
@synthesize configPlist = _configPlist;
@synthesize appleEventDescriptor = _appleEventDescriptor;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{}

- (void)application:(NSApplication *)sender openFiles:(NSArray *)filenames
{
	filesToOpenArray = [[NSMutableArray alloc] initWithArray:filenames];
	[filesToOpenArray sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self openAllTheseFiles:filesToOpenArray];
    filesToOpenArray = nil;
    NSAppleScript *run = [[NSAppleScript alloc] initWithSource:@"tell application \"VMware Fusion\"\nactivate\nend tell"];
	[run executeAndReturnError:nil];
    [[NSApplication sharedApplication] terminate:NULL];
}

- (NSString *)resolveAliasInPath:(NSString *)path
{
	NSString *resolvedPath = nil;
	CFURLRef url = CFURLCreateWithFileSystemPath(NULL, (__bridge CFStringRef)path, kCFURLPOSIXPathStyle, NO);
	
	if (url != NULL) {
		FSRef fsRef;
		if (CFURLGetFSRef(url, &fsRef)) {
			Boolean targetIsFolder, wasAliased;
			if (FSResolveAliasFile (&fsRef, true, &targetIsFolder, &wasAliased) == noErr && wasAliased) {
				CFURLRef resolvedURL = CFURLCreateFromFSRef(NULL, &fsRef);
				if (resolvedURL != NULL) {
					resolvedPath = (__bridge NSString*)CFURLCopyFileSystemPath(resolvedURL, kCFURLPOSIXPathStyle);
				}
			}
		}
	}
	
	if (resolvedPath==nil) {
		return path;
	}
	
	return resolvedPath;
}


- (void)openAllTheseFiles:(NSArray *)arrayOfFiles
{
	NSString *filename;
	for (filename in arrayOfFiles) {
        [self shouldOpen:[self resolveAliasInPath:filename] withEncoding:0];
    }
}

- (void)shouldOpen:(NSString *)path withEncoding:(NSStringEncoding)chosenEncoding
{
    NSString *cfgPath = [[NSBundle mainBundle] bundlePath];
    cfgPath = [cfgPath stringByAppendingPathComponent:@"Contents"];
    cfgPath = [cfgPath stringByAppendingPathComponent:@"Resources"];
    NSString *finalPath = [cfgPath stringByAppendingPathComponent:@"Config.plist"];
    configPlist = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    
    NSAppleEventDescriptor *appleEventSelectionRangeDescriptor = [[[NSAppleEventManager sharedAppleEventManager] currentAppleEvent] paramDescriptorForKeyword:keyAEPosition];
    NSString *lineString;
    if (appleEventSelectionRangeDescriptor) {
        AppleEventSelectionRange selectionRange;
        [[appleEventSelectionRangeDescriptor data] getBytes:&selectionRange length:sizeof(AppleEventSelectionRange)];
         lineString = [NSString stringWithFormat:@"%d", selectionRange.lineNum+1];
    } else {
        lineString = [NSString stringWithFormat:@"%d", -1];
    }
    NSString *vsrunLocation = [configPlist objectForKey:@"VSRunLocation"];
    NSString *vmxFileLocation = [configPlist objectForKey:@"vmxFileLocation"];
    NSString *guestOSUser = [configPlist objectForKey:@"windowsUser"];
    NSString *guestOSPass = [configPlist objectForKey:@"windowsPassword"];
    NSString *guestPath = [configPlist objectForKey:@"folderMappingTo"];
    guestPath = [guestPath stringByAppendingString:path];
    
    //NSLog(@"guest path = %@", guestPath);
    
    //path = [path stringByReplacingOccurrencesOfString:[configPlist objectForKey:@"folderMappingFrom"] withString:[configPlist objectForKey:@"folderMappingTo"]];
    
    NSString *pathToVMRun;
    SInt32 major, minor;
    
    Gestalt(gestaltSystemVersionMajor, &major);
    Gestalt(gestaltSystemVersionMinor, &minor);
    
    if (minor >= 7) {
        pathToVMRun = @"/Applications/VMware\\ Fusion.app/Contents/Library/./vmrun";
    } else {
        pathToVMRun = @"/Library/Application\\ Support/VMware\\ Fusion/./vmrun";
    }
    //NSLog(@"path : %@, pathToVMRun : %@", path,pathToVMRun);
    
    NSMutableString *command = [NSMutableString stringWithString:pathToVMRun];
    [command appendString:@" -T fusion -gu "];
    [command appendString:guestOSUser];
    [command appendString:@" -gp "];
    [command appendString:guestOSPass];
    [command appendString:@" runProgramInGuest \""];
    [command appendString:vmxFileLocation];
    [command appendString:@"\" -activeWindow -interactive \""];
    [command appendString:vsrunLocation];
    [command appendString:@"\" \""];
    [command appendString:guestPath];
    [command appendString:@"\" "];
    [command appendString:lineString];
    //NSLog(@"Sending command to VMware : %@", command);
    system([command UTF8String]);
}




@end


