//
//  main.m
//  VirtualStudio
//
//  Created by Tom Jackson on 19/03/2012.
//  Copyright (c) 2012 Quick Fingers. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
