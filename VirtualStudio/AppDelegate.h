//
//  AppDelegate.h
//  VirtualStudio
//
//  Created by Tom Jackson on 19/03/2012.
//  Copyright (c) 2012 Quick Fingers. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ODBEditorSuite.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

{
    NSMutableArray *filesToOpenArray;
	NSAppleEventDescriptor *appleEventDescriptor;
    NSDictionary *configPlist;
}

- (NSString *)resolveAliasInPath:(NSString *)path;
- (void)openAllTheseFiles:(NSArray *)arrayOfFiles;
- (void)shouldOpen:(NSString *)path withEncoding:(NSStringEncoding)chosenEncoding;

typedef struct _AppleEventSelectionRange {
	short unused1; // 0 (not used)
	short lineNum; // line to select (<0 to specify range)
	long startRange; // start of selection range (if line < 0)
	long endRange; // end of selection range (if line < 0)
	long unused2; // 0 (not used)
	long theDate; // modification date/time
} AppleEventSelectionRange;


@property (assign) IBOutlet NSWindow *window;
@property (readonly) NSMutableArray *filesToOpenArray;
@property (assign) NSAppleEventDescriptor *appleEventDescriptor;
@property (assign) NSDictionary *configPlist;

@end
