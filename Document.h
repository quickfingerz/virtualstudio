//
//  Document.h
//  VirtualStudio
//
//  Created by Tom Jackson on 11/06/2014.
//  Copyright (c) 2014 Quick Fingers. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Document : NSDocument

@end
